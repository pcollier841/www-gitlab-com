---
layout: handbook-page-toc
title: "Infrastructure Security"
description: "GitLab's Infrastructure Security provides security oversight of the SaaS."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Infrastructure Security Overview

GitLab's Infrastructure Security team is responsible for the planning and execution of initiatives specific to the security of GitLab.com. Its core mission is to be [Infrastructure Department](https://about.gitlab.com/handbook/engineering/infrastructure/)'s stable counterpart in Security. This is achieved by sharing an SRE-like view of GitLab.com, but with a narrower security focus. It focuses on infrastructure change reviews,  SaaS infrastructure access & permissions models, operating system hardening, security monitoring at the host and container level, vulnerability management, and patching policies.

Further details can be found in the [job family description](/job-families/engineering/infrastructure-security/).

## Team scope and distinction

The team's mission overlaps with that of several other teams. That being said, it is important to understand how and where these overlaps take place, and how it all fits together.

### Infrastructure Security & Infrastructure Department

The [Infrastructure Department](https://about.gitlab.com/handbook/engineering/infrastructure/) is focused on availability, reliability, performance, and scalability efforts of GitLab.com. The fast pace that's intrinsic to running a rapidly growing SaaS can often prove challenging to secure - operational issues, technical & security debt, rapid implementation of new technologies, all present serious security risks that could impact the success of the SaaS in the long run. This is where Infrastructure Security comes into play by serving the Infrastructure Department in 2 specific modes:

* As an **internal consultancy** to help review and challenge decisions from a security standpoint (i.e. how to improve the security of k8s, what to log, what approach to take to access production environments in a secure and auditable way ...)
* As an **external enabler** that alleviates Infrastructure's load from Security's incessant requests - see the [list of items to be transferred over to InfraSec](#)

The role of the Infratructure Security team can hereby be compared to the role of the Application Security team - the latter helps with the quality of the code, while the former helps with the quality of the infrastructure.

### Infrastructure Security & Security Incident Response Team - SIRT 

The [Security Incident Response Team - SIRT](https://about.gitlab.com/handbook/engineering/security/security-operations/sirt/sec-incident-response.html) has historically been the catch-all for most security issues at GitLab. As a result, over the years, SIRT ended up being a temporary owner for many non-SIRT responsibilities. With the introduction of Infrastructure Security, some of these responsibilities have been shifted to this new team. Good examples of some of these responsibilities are vulnerability management and security monitoring.

SIRT's goal is detection and response of anomalies and security events - on the SaaS and on the corporate side of GitLab. As such, SIRT is a very strong partner to Infrastructure Security.

## Tech stack & process ownership

The end goal is to have existing aspect of security at GitLab transfer over to Infrastructure Security as the new designated owner:

* Infrastructure vulnerability management (from SIRT)
* Infrastrucure patching policies (from Infrastructure & SIRT)
* Pipeline security & 3rd party scanners (from Application Security)
* Deployment, tuning, and maintenance of GitLab.com security monitoring - VM and containers (from Infrastructure & SIRT)
* Security's log shipping pipeline and infrastructure (from SIRT)
* CDN & WAF (from Infrastructure & SIRT)
* Security's dedicated GCP environment - Enclave (from SIRT)

## Onboarding 

TBD

## Baseline entitlements

TBD
